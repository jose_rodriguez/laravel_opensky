<?php

use Illuminate\Support\Facades\Route;
use GuzzleHttp\Client;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    $endpoint = "https://opensky-network.org/api/flights/arrival";
    $client = new \GuzzleHttp\Client();
    $id = 5;
    $value = "ABC";

    $response = $client->request('GET', $endpoint, ['query' => [
        'airport' => 'EDDF', 
        'begin'   => 1517227200,
        'end'     => 1517230800
    ]]);

    
    $statusCode = $response->getStatusCode();
    $content = $response->getBody();

    // Respuesta JSON
    $content = json_decode($response->getBody(), true);
    //Mostramos resultados a modo de test
    dd($content);

    return view('welcome');
});
